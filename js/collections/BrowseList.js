define(['backbone', 'models/Browse', 'extensions/Utils',
        'backbone.paginator'],
        function(Backbone, Browse, Utils) {
  'use strict';

  return Backbone.Paginator.requestPager.extend({
    initialize: function(init_models, options) {
      this.datasets = options.datasets;
    },
    model: Browse,

    paginator_core: {
      type: 'GET',
      dataType: 'jsonp',
      url: function() {
        return Utils.proxy('dataset/search/?');
      }
    },

    paginator_ui: {
      firstPage: 1,
      currentPage: 1,
      perPage: 15,
      totalPages: 138,
    },

    server_api: {
      'page': function() {
        return this.currentPage;
      },
      'reporters': function() {
        return [(this.datasets.at(0).get('reporters') || []), (this.datasets.at(0).get('dataset_id') || 0)].join(',');
      },
      'q': function() {
        return $('#searchDS').val() || '';
      },
      'callback' : '?'
    },

    parse: function(res) {
      console.log('P A R S E');
      var results = res[2];

      this.perPage = Number(results.length);
      //-- this.totalPages = Math.ceil( res[0] / results.length );
      this.totalPages = res[1];
      this.totalRecords = Number(res[0]);
      console.log('results', results);
      return results;
    }

  });
});
