define(['marionette', 'templates', 'vent',
        'd3'], 
      function (Marionette, templates, vent) {
  'use strict';

  return Marionette.ItemView.extend({
    template : templates.chart.views.graph,

    ui : {
      graph : 'div.datachart',
      svg   : 'svg',
    },

    onRender : function() {
      var self = this,
          viz = self.model.get('viz');
      //-- Draw it to the screen
      viz.$el = self.ui.graph;
      viz.size = {
        width: $(window).width(),
        height: 400,
      };

      //-- Calculated for init draw
      viz.init();
      viz.drawWindowGroups();
      viz.refresh();
      viz.refresh();

      $(window).resize(function(evt) {
        viz.size = {
          width:  self.ui.graph.width(),
          height: 400,
        }
        viz.refresh();
        $('svg').attr({ version: '1.1' , xmlns:"http://www.w3.org/2000/svg"});
        self.model.set('html', self.ui.graph.html() );
      });

      $('svg').attr({ version: '1.1' , xmlns:"http://www.w3.org/2000/svg"});
      self.model.set('html', self.ui.graph.html() );

    },

  });
});
