define(['marionette', 'templates', 'vent',

        'views/Chart/Options/List', 'models/Option', 'collections/OptionList'], 
    function (Marionette, templates, vent,
              List, Option, Options) {
  'use strict';

  return Marionette.Layout.extend({
    template : templates.chart.modal.options_list,
    templateHelpers : function() { return this.options; },

    regions: {
      list   : 'tbody',
    },

    initialize : function(options) {
      var factors = options.model.get('DisplaySettings').get('factors'),
          colors = options.model.get('DisplaySettings').get('colors'),
          models = [];

      _.each( factors[options.factor], function(factors_option) {
        models.push(new Option({
          factor: factors_option, 
          color: colors[options.factor][factors_option]
        }));
      });

      options.collection = new Options(models);
    },

    onRender : function() {
      this.list.show( new List(this.options) )
    },

  });
});
