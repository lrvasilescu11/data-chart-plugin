define(['extensions/Search'], 
    function (search) {
  'use strict';

  return {
    cartesianOdometer : function(vals, model) {
      var val_obj = model.get('factors'),
          args = [];
      _.each(vals, function(v) { args.push(val_obj[v]); });

      var state = [], //-- state of option iterator for each index
          max = [], //-- ceil int of options for that index
          level = 0,
          i,
          l = args.length,
          combinations = {},
          combo = [];

      //-- Set the range of the odometer
      for (i=0; i<l; i++) {
        state.push(0);
        max.push(args[i].length - 1);
      }

      while (true) {
        combo = [];
        for (i=0; i<l; i++) { combo.push( args[i][state[i]] ); };
        combinations[ combo.join('') ] = level;

        state[0]++;
        for (i=0; i<l; i++) {
          if (state[i] > max[i]) {
            if (i === l - 1) { return combinations; };
            state[i] = 0;
            state[i+1]++;
          } else { break; }
        }
        level++;
      }
    },

    aggregate_on : function(model) {
      var aggregate_on = [];

      //-- Retrieve based off obj
      _.each(model.get('order'), function(factor) {
        if( model.get('aggregate')[factor] ) {
          aggregate_on.push(factor);
        }
      })
      return aggregate_on;
    },

    byFactor : function(model) {
      var search_obj = this.cartesianOdometer( model.get('display_settings') );
      return search.dataset(search_obj, model.get('display_settings').order, model.get('data'), false )
    }
  }
});
