define(['extensions/Aggregate'], 
    function (aggregate) {
  'use strict';

  return {
    dataset : function(search_obj, factor_order, data, agg) {
      //-- object of option indexes, set order for the factors, cloned data, group or leave individual?
      _.each(data, function(sample) {
        var factors = sample['factors'],
            search = [];
          _.each(factor_order, function(factor_key) { 
            search.push(factors[ factor_key ]);
          })
          sample['order'] = search_obj[ search.join('') ];
      });

      if(agg) {
        var new_samples = [],
            //-- sorted = _.sortBy(data,     function(sample) { return sample.order; }),
            //-- Don't need to sort as groupBy will return a sorted array
            //-- of arrays based on the groupBy comparator
            groups = _.groupBy(data,  function(sample) { return sample.order; });
        _.each(groups, function(group) {
          new_samples.push( aggregate.group(group, factor_order) );
        });
        return _.flatten(new_samples);

      } else { return _.sortBy(data, function(sample) { return sample.order; }); };

    },
  }
});
