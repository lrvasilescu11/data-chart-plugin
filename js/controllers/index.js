define(['vent'], 
        function (vent) {
  'use strict';

  return {
    wildcardRoute : function(param) {
      vent.trigger('dataset:load', ['1017', undefined]);
    },

    showDefault : function(entrez_id, dataset_id) {
      vent.trigger('dataset:load', [entrez_id, dataset_id]);
    }

  };
});
